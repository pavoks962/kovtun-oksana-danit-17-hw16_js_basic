"use strict"

let firstNumber;

let nextNumber;

let n;


const validateNumber = number => (number === null || number === '' || isNaN(number));

const getNumber = (m, message = "Enter any number for calculate fibonacci") => {
  do {
    m = prompt(message);
  } while (validateNumber(m));
  return +m;
}

firstNumber = getNumber(firstNumber, 'Enter any number for calculate fibonacci "F0"');
nextNumber = getNumber(nextNumber, 'Enter any number for calculate fibonacci "F1" than "F0"');
n = getNumber(n, 'Enter ordinal number Fibonacci "n"');


const calcFibonacci = (n) => {
  for (let i = 1; i <= n; i++) {
    let result;
    result = nextNumber;
    nextNumber = firstNumber + nextNumber
    firstNumber = result;

  } return nextNumber;
}


const calcFibonacciNegative = (n) => {
  for (let i = 1; i >= n; i--) {
    let result;
    result = firstNumber;
    firstNumber = nextNumber - firstNumber;
    nextNumber = result;

  } return nextNumber;
}


const printResult = n => (n > 0) ? calcFibonacci(n) : calcFibonacciNegative(n)

console.log(printResult(n));


// розрахунок методом рекурсії функції (позитивне n рахується, від'ємне n  не рахується. )

// let result;

// const calcFibonacci = n => {

// result = firstNumber + nextNumber;
// if (n === 0) {
//   return nextNumber;
// } if ( n === 1) {
//   return result;
// } else {
//   return calcFibonacci(n-1) + calcFibonacci(n-2);
// }
// }


// const calcFibonacciNegative = n => {

//   result = nextNumber - firstNumber;
//   if (n <= 0) {
//     return firstNumber;
//   } if ( n === 1) {
//     return result;
//   } else {
//     return calcFibonacci(n+2) - calcFibonacci(n+1);
//   }
//   }


